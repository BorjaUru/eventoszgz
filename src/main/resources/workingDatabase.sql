#drops
DROP DATABASE 'inventario';

#creates
CREATE DATABASE  IF NOT EXISTS inventario;
use inventario;

CREATE TABLE locations (
    id_location INT PRIMARY KEY,
    place VARCHAR(100) utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'no place',
    public_transport VARCHAR(250) NULL,
    address VARCHAR(100) NULL DEFAULT 'no address',
    postal_code VARCHAR(5) NULL DEFAULT '00000');

CREATE TABLE users(
    id INT  PRIMARY KEY AUTO_INCREMENT,
    nick VARCHAR(45) NOT NULL,
    pass VARCHAR(45) NOT NULL,
    role INT NOT NULL);
CREATE TABLE cultural_events
(
    id_event       INT PRIMARY KEY,
    title          VARCHAR(200) NOT NULL,
    category       VARCHAR(45)  NULL DEFAULT 'no category',
    start_date     DATE         NULL DEFAULT CURDATE(),
    end_date       DATE         NULL DEFAULT CURDATE(),
    population     VARCHAR(45)  NULL DEFAULT 'no population',
    image_url      VARCHAR(200) NULL DEFAULT 'no image url',
    lat_coordinate DOUBLE       NULL DEFAULT NULL,
    lon_coordinate DOUBLE       NULL DEFAULT NULL,
    id_location    INT          NULL DEFAULT NULL,
    description    VARCHAR(200) NULL DEFAULT NULL,
    min_age        INT          NULL DEFAULT NULL,
    max_age        INT          NULL DEFAULT NULL,
    #INDEX (id_location),
    FOREIGN KEY (id_location)
        REFERENCES locations (id_location)
    #ON DELETE CASCADE ON UPDATE CASCADE);
);

# safe mode off:
# set sql_safe_updates = 0;

# del locations
# delete from inventario.locations;
