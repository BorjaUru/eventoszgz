package beans;

import lombok.Data;

import javax.persistence.*;
import java.net.URL;

@Data

@Entity @Table(name="cultural_events", schema = "inventario")
public class CulturalEvent {

    @Id
    @Column(name="id_event", nullable=false)
    private long idEvent;

    @Column(name="title", nullable=false)
    private String title;

    @Column(name="category")
    private String category;

    @Column(name="start_date")
    private String startDate;

    @Column(name="end_date")
    private String endDate;

    @Column(name="population")
    private String population;

    @Column(name="image_url")
    private URL imageUrl;

    @Column(name="lat_coordinate")
    private double latCoordinate;

    @Column(name="lon_coordinate")
    private double lonCoordinate;

    @Column (name="description")
    private String description;

    @Column (name="min_age")
    private int minAge;

    @Column (name="max_age")
    private int maxAge;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_location", nullable = false)
    private Location location;
}
