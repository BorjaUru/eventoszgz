package beans;

import lombok.Data;

@Data
public class Filter {
    private String population;
    private String category;
    private String postalCode;
    private String title;
    private String startDate;
    private int numPage;
    private int limitPerPage;
    private int lastPage;
}
