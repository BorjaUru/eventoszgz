package beans;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity @Table(name="users", schema = "inventario")
public class User {
    @Id
    @Column(name="id", nullable=false)
    private int id;
    @Column(name="nick", nullable=false)
    private String nick;
    @Column(name="pass", nullable=false)
    private String pass;
    @Column(name="role", nullable=false)
    private int role;
}
