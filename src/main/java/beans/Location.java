package beans;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity @Table(name="locations", schema = "inventario")
public class Location {

    @Id
    @Column(name="id_location")
    private long idLocation;

    @Column(name="place")
    private String place;

    @Column(name="public_transport")
    private String publicTransport;

    @Column(name="address")
    private String address;

    @Column(name="postal_code")
    private String postalCode;
}
