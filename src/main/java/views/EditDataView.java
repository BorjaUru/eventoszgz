package views;

import javax.swing.*;
import java.awt.event.*;
import java.util.Date;

import beans.CulturalEvent;

public class EditDataView extends JDialog {
    private JPanel contentPane;
    private JButton buttonSave;
    private JButton buttonCancel;
    private JTextField txtTitle;
    private JTextField txtCategory;
    private JTextField txtStartDate;
    private JTextField txtEndDate;
    private JTextField txtLocation;

    private CulturalEvent culturalEvent;

    public EditDataView () {
        setContentPane(contentPane);
        setTitle("Editing Data");
        setModal(true);
        getRootPane().setDefaultButton(buttonSave);

        buttonSave.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed (ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK () {
        // add your code here
        if(checkFields()) {
            culturalEvent = new CulturalEvent();
            culturalEvent.setTitle(txtTitle.getText());
            culturalEvent.setCategory(txtCategory.getText());
            //culturalEvent.setStartDate(txtStartDate.getText());
            //culturalEvent.setEndDate(txtEndDate.getText());
            // culturalEvent.setLocation(txtLocation.getText());
        }
        dispose();
    }

    private boolean checkFields(){
        return txtTitle.getText() != null && !txtTitle.getText().equals("") &&
        txtCategory.getText() != null && !txtCategory.getText().equals("") &&
        txtStartDate.getText() != null && !txtStartDate.getText().equals("") &&
        txtEndDate.getText() != null && !txtEndDate.getText().equals("") &&
        txtLocation.getText() != null && !txtLocation.getText().equals("");
    }

    private void onCancel () {
        // add your code here if necessary
        dispose();
    }

    public static void main (String[] args) {
        EditDataView dialog = new EditDataView();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
