package views;

import beans.CulturalEvent;
import beans.Filter;
import com.toedter.calendar.JDateChooser;
import controllers.ListController;
import controllers.ListControllerImpl;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;


public class ListView extends JFrame{
    private ListController controlador=new ListControllerImpl();
    private JPanel panel1;
    private JComboBox comboBox3;
    private JComboBox comboBox4;
    private JTextField textField1;
    private JTable table1;
    private JButton jbAdd;
    private JDateChooser JDateChooser1;
    private JLabel lFoto;
    private JButton consultarButton;
    private JButton returnButton;
    private JCheckBox freeCheckBox;
    private CulturalEvent seleccionado;
    private ImageIcon imageIcon;
    private List<CulturalEvent> cutalEvents;

    public ListView(Filter filter){
        initComponents();
        actualizarFiltros();
        //selecionarFiltros(filter);
        actulizarTabla();
    }

    private void selecionarFiltros(Filter f) {
        comboBox3.setSelectedItem(f.getPopulation());
        textField1.setText(f.getTitle());
        comboBox4.setSelectedItem(f.getCategory());
        //freeCheckBox.setSelected(f.isFree());
        //JDateChooser1.setDate(f.getStartDate());
    }

    private void initComponents() {
        setContentPane(panel1);
        freeCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actulizarTabla();
            }
        });
        textField1.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                actulizarTabla();
            }
        });
        comboBox3.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actulizarTabla();
            }
        });
        comboBox4.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actulizarTabla();
            }
        });
        JDateChooser1.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                actulizarTabla();
            }
        });
        consultarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openDialog(seleccionado);
            }
        });
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                returnView();
            }
        });
        jbAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                HashMap<String, Object> res=controlador.add();
                if (res.get("redirect")!=null){
                    if ("CulturalEventInsertView".equals(res.get("redirect"))){

                       openDialog(new CulturalEvent());
                    }
                }

            }
        });

        lFoto.setIcon(imageIcon);
        JButton a = new JButton("");
        a.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
        table1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                seleccionado = cutalEvents.get(table1.getSelectedRow());
                if(seleccionado.getImageUrl()!=null){
                    imageIcon = new ImageIcon(new  ImageIcon(seleccionado.getImageUrl()).getImage().getScaledInstance(300, 300,  java.awt.Image.SCALE_SMOOTH));
                }
                consultarButton.setEnabled(true);
            }
        });

    }

    private void createUIComponents() {
        JDateChooser1 = new JDateChooser();
    }

    private void openDialog(CulturalEvent culturalEvent){
       //EditDataView edv = new EditDataView(sesion,culturalEvent);
       //edv.setVisible(true);
    }
    private void returnView(){
        //homeView hv = new homeView(sesion);
        //hv.setVisible(true);
        this.dispose();
    }

    private void actulizarTabla(){
        Filter f = new Filter();
        //f.setPopulation(comboBox3.getSelectedItem().toString());
        f.setTitle(textField1.getText());
        //f.setCategory(comboBox4.getSelectedItem().toString());
        //f.setFree(freeCheckBox.isSelected());
        //f.setStartDate(JDateChooser1.getDate());
        HashMap<String, Object> res=controlador.loadList(f);
        if (res.get("error")!=null){
            JOptionPane.showMessageDialog(this, res.get("error"));
        }
        if (res.get("dataListaCulturalEvent")!=null){
            cutalEvents = (List<CulturalEvent>)res.get("dataListaCulturalEvent");

            DefaultTableModel dtm =new DefaultTableModel();
            dtm.addColumn("Title");
            dtm.addColumn("Category");
            dtm.addColumn("Population");
            dtm.addColumn("Start Date");
            for(CulturalEvent ce:cutalEvents){
                dtm.addRow(new Object[]{ce.getTitle(),ce.getCategory(),ce.getPopulation(),ce.getStartDate()});

            }
            table1.setModel(dtm);

        }
    }
    private void actualizarFiltros(){
        comboBox3.removeAllItems();
        comboBox4.removeAllItems();
        HashMap<String, Object> res=controlador.loadFilters();
        if (res.get("error")!=null){
            JOptionPane.showMessageDialog(this, res.get("error"));
        }
        if (res.get("dataPopulation")!=null){
            List<String> lista = (List<String>)res.get("dataPopulation");
            for (String item:lista) {
                comboBox3.addItem(item);
            }
        }
        if (res.get("dataCategory")!=null){
            List<String> lista = (List<String>)res.get("dataCategory");
            for (String item:lista) {
                comboBox4.addItem(item);
            }
        }
    }

}
