package controllers;

import beans.CulturalEvent;
import com.google.gson.Gson;
import models.service.CulturalEventService;
import models.service.CulturalEventServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@WebServlet("/Index/*")
public class IndexControllerServlet extends HttpServlet {

    private CulturalEventService culturalEvent = new CulturalEventServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI();
        response.setContentType("application/json;charset=UTF-8");
        if (url.endsWith("/loadEventsToday")) {
            loadEventsToday(request, response);
        }
        if (url.endsWith("/loadEventsForDate")) {
            loadEventsForDay(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void loadEventsToday(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HashMap<String, Object> res = new HashMap<String, Object>();

        try {
            List<CulturalEvent> lista = culturalEvent.getCulturalEventTodayList();
            res.put("eventList", lista);
        } catch (Exception e) {
            res.put("error", "Error: " + e.getMessage());
        }

        PrintWriter out = response.getWriter();
        String json = new Gson().toJson(res);
        out.print(json);
    }

    protected void loadEventsForDay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> res = new HashMap<String, Object>();
        Date date = new Date(request.getParameter("date"));

        try {
            List<CulturalEvent> lista = culturalEvent.getCulturalEventForDateList(date);
            res.put("eventList", lista);
        } catch (Exception e) {
            res.put("error", "Error: " + e.getMessage());
        }

        PrintWriter out = response.getWriter();
        String json = new Gson().toJson(res);
        out.print(json);
    }

}
