package controllers;

import beans.Filter;

import java.util.HashMap;

public interface ListController {
    public HashMap<String, Object> loadFilters();
    public HashMap<String,Object> loadList(Filter filter);
    public HashMap<String, Object> add();
    public HashMap<String, Object> borrarEvent(int id);

}
