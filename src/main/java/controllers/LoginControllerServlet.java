package controllers;

import beans.User;
import com.google.gson.Gson;
import models.service.LoginService;
import models.service.LoginServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@WebServlet("/Login/*")
public class LoginControllerServlet extends HttpServlet {

    LoginService loginService = new LoginServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI();
        if (url.endsWith("/login")) {
            login(request, response);
        } else if (url.endsWith("/userSession") ){
            changeUserSession(request,response);
        } else if (url.endsWith("/logout") ){
            logout(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void login (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        HashMap<String, Object> res = new HashMap<>();
        String user = req.getParameter("user");
        String pass = req.getParameter("pass");

        try{
            User userLogin =loginService.checkLogin(user,pass);
            req.getSession().setAttribute("userSession",userLogin);
            res.put("redirect","return");
            res.put("userSesion",req.getSession().getAttribute("userSession"));
        } catch (Exception e){
            res.put("error", "Error: " + e.getMessage());
        }
        PrintWriter out = resp.getWriter();
        String json = new Gson().toJson(res);
        out.print(json);
    }

    protected void changeUserSession (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        HashMap<String, Object> res = new HashMap<>();
        res.put("userSesion",req.getSession().getAttribute("userSession"));
        PrintWriter out = resp.getWriter();
        String json = new Gson().toJson(res);
        out.print(json);
    }

    protected void logout (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        HashMap<String, Object> res = new HashMap<>();
        User user = new User();
        user.setId(1);
        user.setNick("common");
        user.setPass("common");
        user.setRole(0);
        req.getSession().setAttribute("userSession",user);
        res.put("reload","reloadPage");
        PrintWriter out = resp.getWriter();
        String json = new Gson().toJson(res);
        out.print(json);
    }



}