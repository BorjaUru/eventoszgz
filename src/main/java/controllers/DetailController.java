package controllers;

import java.util.HashMap;

public interface DetailController {

    // "error" --> String
    // "dataEvent" --> CulturalEvent
    // "datauser" --> User
    public HashMap<String, Object> load(String type);

    public HashMap<String, Object> getCulturalEvent(int id);

    public HashMap<String, Object> insertCulturalEvent();

    public HashMap<String, Object> editCulturalEvent();

    public HashMap<String, Object> deleteCulturalEvent();

}
