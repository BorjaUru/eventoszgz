package controllers;

import beans.CulturalEvent;
import beans.Filter;
import beans.Location;
import com.google.gson.Gson;
import models.service.CulturalEventService;
import models.service.CulturalEventServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@WebServlet("/DetailControllerServlet/*")
public class DetailControllerImpl extends HttpServlet implements DetailController {
    private boolean ok = false;
    private String mode = "";

    private CulturalEventService eventService = new CulturalEventServiceImpl();
    CulturalEvent eventToLoad = null;

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet (HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HashMap<String, Object> hashMap = new HashMap<>();
        // ACTION: SEARCH - SAVE - DELETE
        String action = req.getParameter("todo");
        switch (action){
            case "search":
                hashMap = search(req, resp);
                break;
            case "save":
                hashMap = save(req, resp);
                break;
            case "delete":
                hashMap = delete(req, resp);
                break;
            default:
        }
        resp.getWriter().println(new Gson().toJson(hashMap));

        // para controlar varios botones por url
        // String url = req.getRequestURL().toString();
        // if (url.endsWith("/load")) {
        //     load(req, resp);
        // }
    }

    private void loadEvent(HttpServletRequest req, String idSession)
            throws IOException{
        if(!idSession.isEmpty()) {
            try {
                long id = Long.parseLong(idSession);
                eventToLoad = new CulturalEventServiceImpl().getCulturalEvent(id);
                mode = "visualization";
                ok = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            eventToLoad = new CulturalEvent();
            eventToLoad.setImageUrl(new URL(
                    "https://pbs.twimg.com/profile_images/993967511099068416/F3HK-M-4_400x400.jpg"));
            mode = "creation";
        }
    }

    private void createEvent(HttpServletRequest req){
        CulturalEvent ev = null;
        Location loc = null;
        if (req.getParameter("id_event") == null || req.getParameter("id_event").equals("0")){
            eventToLoad = new CulturalEvent();
            loc = new Location();
            loc.setIdLocation(myRandom11());
            eventToLoad.setIdEvent(myRandom11());
        }else{
            loc = eventToLoad.getLocation();
            eventToLoad.setIdEvent(Long.parseLong(req.getParameter("id_event")));
        }
        loc.setPlace(req.getParameter("place"));
        loc.setPublicTransport(req.getParameter("transport"));
        loc.setAddress(req.getParameter("address"));
        loc.setPostalCode(req.getParameter("postal_code"));

        eventToLoad.setLocation(loc);
        eventToLoad.setDescription(req.getParameter("description"));
        eventToLoad.setTitle(req.getParameter("title_event"));
        eventToLoad.setCategory(req.getParameter("category"));
        eventToLoad.setPopulation(req.getParameter("population"));
        eventToLoad.setStartDate(req.getParameter("start_date"));
        eventToLoad.setEndDate(req.getParameter("end_date"));
    }

    /** SEARCH */// TODO check
    private HashMap<String, Object> search(HttpServletRequest req, HttpServletResponse resp)
            throws IOException{
        ok = false;
        HttpSession session = req.getSession(false);
        String idEventSession = session.getAttribute("idEv").toString();
        HashMap<String, Object> hashMap = new HashMap<>();
        loadEvent(req, idEventSession);
        hashMap.put("ok", ok);
        hashMap.put("mode", mode);
        hashMap.put("ev", eventToLoad);
        return hashMap;
    }

    /** SAVE */ // TODO implement
    private HashMap<String, Object> save(HttpServletRequest req, HttpServletResponse resp)
            throws IOException{
        ok = false;
        long id = 0;
        HttpSession session = req.getSession(false);
        String sessionEvent = session.getAttribute("idEv").toString();
        HashMap<String, Object> hashMap = new HashMap<>();
        // modifying existent
        // if(!sessionEvent.isEmpty()){
        //     loadEvent(req, sessionEvent);
        //     try {
        //         eventService.insertCulturalEvent(eventToLoad);
        //         ok = true;
        //     }catch(Exception e){
        //         e.printStackTrace();
        //     }
        // }// creating new one
        // else{
        //     createEvent(req);
        //     try {
        //         eventService.insertCulturalEvent(eventToLoad);
        //         ok = true;
        //     }catch (Exception e){ e.printStackTrace();}
        // }

        createEvent(req);
        try {
            eventService.insertCulturalEvent(eventToLoad);
            ok = true;
        }catch (Exception e){ e.printStackTrace();}

        hashMap.put("ok", ok);
        hashMap.put("mode", mode);
        hashMap.put("ev", eventToLoad);
        return hashMap;
    }

    /** DELETE */ // TODO implement
    private HashMap<String, Object> delete(HttpServletRequest req, HttpServletResponse resp){
        HashMap<String, Object> hashMap = new HashMap<>();

        return hashMap;
    }

    // obsolete methods - desktop view
    public HashMap<String, Object> load (String type) {
        HashMap<String, Object> resHash = new HashMap<>();
        List<CulturalEvent> listEvents = null;
        try{
            listEvents = eventService.filterCulturalEvent(new Filter());
        }catch(Exception e){e.printStackTrace();}
        resHash.put("dataEvent", listEvents);

        return resHash;
    }
    public HashMap<String, Object> getCulturalEvent (int id) {
        HashMap<String, Object> resHash = new HashMap<String, Object>();
        CulturalEvent event = null;
        try {
            event = eventService.getCulturalEvent(id);
        }catch (Exception e){e.printStackTrace();}
        resHash.put("dataEvent", event);
        return resHash;
    }
    public HashMap<String, Object> insertCulturalEvent () {
        return null;
    }
    public HashMap<String, Object> editCulturalEvent () {
        return null;
    }
    public HashMap<String, Object> deleteCulturalEvent () {
        return null;
    }

    private long myRandom11(){
        long leftLimit = 1L;
        long rightLimit = 99999999L;
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }
}
