/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.CulturalEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import models.service.CulturalEventServiceImpl;
import models.service.CulturalEventService;

/**
 *
 * @author acordero
 */
public class HomeControllerImpl implements HomeController {
    CulturalEventService culturalEventService=new CulturalEventServiceImpl();

    public HashMap<String, Object> loadCulturalEventsToday() {
        List<CulturalEvent> list = null;
        try {
            list = culturalEventService.getCulturalEventTodayList();
        }catch(Exception e){e.printStackTrace();}
        HashMap<String,Object> res=new HashMap<String,Object> ();
        res.put("events", list);
        return res;
    }

}
