package controllers;

import beans.CulturalEvent;
import beans.Filter;
import com.google.gson.Gson;
import models.service.CulturalEventServiceImpl;
import models.service.CulturalEventService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

@WebServlet("/ListControllerImpl/*")
public class ListControllerImpl extends HttpServlet implements ListController{
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListControllerImpl() {
        super();
    }

    private CulturalEventService culturalEventService= new CulturalEventServiceImpl();


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Filter f=(Filter)session.getAttribute("filter");
        HashMap<String, Object> res= new HashMap<>();
        res.put("error","error");
        if(request.getRequestURI().endsWith("/cargar")){
           /* if(request.getParameter("reset")!=null && !"0".equals(request.getParameter("reset"))){
                f.setNumPage(1);
                f.setStartDate("");
                f.setTitle("");
                f.setCategory("");
                f.setPopulation("");
                f.setPostalCode("");
            }
            */
            res = new HashMap<>();
            HashMap<String, Object> b=loadFilters();
            res.put("dataCategory", b.get("dataCategory"));
            res.put("dataPopulation", b.get("dataPopulation"));
            b=loadList(f);
            res.put("dataListaCulturalEvent", b.get("dataListaCulturalEvent"));
        }else if(request.getRequestURI().endsWith("/ver")){
            session.setAttribute("idEv", request.getParameter("theid"));
            res= new HashMap<>();
            res.put("ok","ok");
        }else if(request.getRequestURI().endsWith("/borrar")){
            res = borrarEvent(Integer.parseInt(request.getParameter("theid")));
            HashMap<String, Object> c = loadList(f);
            res.put("dataListaCulturalEvent",c.get("dataListaCulturalEvent"));

        }else if(request.getRequestURI().endsWith("/filtrar")){
            f.setNumPage(1);
            f.setTitle(request.getParameter("title"));
            f.setCategory(request.getParameter("category"));
            f.setPopulation(request.getParameter("population"));
            f.setPostalCode(request.getParameter("cp"));
            f.setStartDate(request.getParameter("fecha"));
            res = loadList(f);
        }else if(request.getRequestURI().endsWith("/pag")){
            f.setNumPage(Integer.parseInt(request.getParameter("numPag")));
            res = loadList(f);
        }else if(request.getRequestURI().endsWith("/changePag")){
            f.setNumPage(1);
            f.setLimitPerPage(Integer.parseInt(request.getParameter("limitPerPage")));
            res = loadList(f);
        }
        res.put("filter",f);
        response.setContentType("application/json; charset=UTF-8");
        PrintWriter o = response.getWriter();

        o.print(new Gson().toJson(res));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }


    public HashMap<String, Object> loadFilters() {
        HashMap<String, Object> res=new HashMap<String, Object>();
        try{
            List<String> lista= culturalEventService.filterCategory();
            res.put("dataCategory", lista);
        }catch(Exception e){
            res.put("error", "Ha habido un error inexperidado");
        }
        try{
            List<String> lista= culturalEventService.filterPopulation();
            res.put("dataPopulation", lista);
        }catch(Exception e){
            res.put("error", "Ha habido un error inexperidado");
        }
        return res;
    }

    public HashMap<String, Object> loadList(Filter filter) {
        HashMap<String, Object> res=new HashMap<String, Object>();
        try{
            List<CulturalEvent> lista= culturalEventService.filterCulturalEvent(filter);
            res.put("dataListaCulturalEvent", lista);
        }catch(Exception e){
            res.put("error", "Ha habido un error inexperidado");
        }
        return res;
    }
    public HashMap<String, Object> borrarEvent(int id) {
        HashMap<String, Object> res = new HashMap<String, Object>();
        try {
            culturalEventService.deleteCulturalEvent(culturalEventService.getCulturalEvent(id));

        } catch (Exception e) {
            res.put("error", "Ha habido un error inexperidado");
        }
        return res;
    }
    public HashMap<String, Object> add() {
        HashMap<String, Object> res=new HashMap<String, Object> ();
        res.put("redirect", "CulturalEventInsertView");
        return res;
    }

    public HashMap<String, Object> consult() {
        HashMap<String, Object> res=new HashMap<String, Object> ();
        res.put("redirect", "CulturalEventConsultView\"");
        return res;
    }

}

