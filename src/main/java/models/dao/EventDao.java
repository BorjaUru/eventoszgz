package models.dao;

import beans.CulturalEvent;
import beans.Filter;

import java.util.List;

public interface EventDao extends Dao<CulturalEvent> {

    List<CulturalEvent> getListParam(Filter filter);

    List<String> getListCategory();

    List<String> getListPopulation();

    List<CulturalEvent> findCulturalEventToday(String date) throws Exception;

}
