package models.dao;

import beans.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("EventsPersistence");
    private EntityManager entityManager;

    @Override
    public List<User> checkLogin(String nick, String pass) throws Exception {

        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;
        List<User> users = new ArrayList<>();

        try {
            transaction = manager.getTransaction();
            transaction.begin();

            Query query = manager.createQuery("Select u from beans.User u where u.nick = :n and u.pass = :p",User.class);
            query.setParameter("n",nick);
            query.setParameter("p",pass);
            users = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
        return users;
    }
}