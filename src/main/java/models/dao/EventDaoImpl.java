package models.dao;

// TODO IMPLEMENT PATTERN OR DELETE

import beans.CulturalEvent;
import beans.Filter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

public class EventDaoImpl implements EventDao {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("EventsPersistence");
    private EntityManager entityManager;

    /** SEARCHS */

    @Override
    public Optional<CulturalEvent> get (long id) {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        return Optional.ofNullable(entityManager.find(CulturalEvent.class, id));
    }

    @Override
    public List<CulturalEvent> getAll () {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Query query = entityManager.createQuery("SELECT e FROM CulturalEvent e");
        return query.getResultList();
    }

    @Override
    public List<CulturalEvent> findCulturalEventToday(String date) throws Exception {

        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;
        List<CulturalEvent> list = new ArrayList<>();

        try {
            transaction = manager.getTransaction();
            transaction.begin();
            String hql = "select * from inventario.cultural_events ce " +
                    "where (" +
                    "(DATE_FORMAT(ce.start_date,'%d-%m-%Y') = :date1 )" +
                    "or " +
                    "(" +
                    "(DATE_FORMAT(ce.start_date,'%d-%m-%Y') <= :date1 )" +
                    "and " +
                    "(DATE_FORMAT(ce.end_date,'%d-%m-%Y') >= :date1 )" +
                    ")" +
                    ")";
            Query query = manager.createNativeQuery(hql, CulturalEvent.class);
            query.setParameter("date1", date);
            list = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.close();
        }
        return list;
    }

    /** INSERTS */

    @Override
    public void save(CulturalEvent event) {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        executeInsideTransaction(entityManager -> entityManager.merge(event));
    }

    // public void save(List<CulturalEvent> list){
    //     entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
    //     for(CulturalEvent event : list){
    //         executeInsideTransaction(entityManager -> entityManager.merge(event));
    //     }
    // }

    /** UPDATES */

    @Override
    public void update(CulturalEvent event, String[] params) {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        event.setCategory(Objects.requireNonNull(params[0], "Category cannot be null"));
        event.setPopulation(Objects.requireNonNull(params[1], "Population cannot be null"));
        executeInsideTransaction(entityManager -> entityManager.merge(event));
    }

    /** DELETES */

    @Override
    public void delete(CulturalEvent event) {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        CulturalEvent ev = entityManager.find(CulturalEvent.class, event.getIdEvent());
        executeInsideTransaction(entityManager -> entityManager.remove(ev));
    }

    // public void delete(List<CulturalEvent> deleteList){
    //     entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
    //     for(CulturalEvent event : deleteList){
    //         executeInsideTransaction(entityManager -> entityManager.remove(event));
    //     }
    // }

    private void executeInsideTransaction(Consumer<EntityManager> action){
        EntityTransaction transaction = entityManager.getTransaction();
        try{
            transaction.begin();
            action.accept(entityManager);
            transaction.commit();
        }
        catch (RuntimeException e) {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    public List<CulturalEvent> getListParam (Filter f) {
        f.setLastPage(lastPage(f));
        if(f.getNumPage()==-1){
            f.setNumPage(f.getLastPage());
        }
        StringBuilder queryString = new StringBuilder("FROM CulturalEvent e");
        addWhere(queryString,f);
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Query query = entityManager.createQuery(queryString.toString())
                .setFirstResult((f.getLimitPerPage()*f.getNumPage())-f.getLimitPerPage())
                .setMaxResults(f.getLimitPerPage());
        remplaceParameters(query,f);
        return query.getResultList();
    }
    private int lastPage(Filter f){
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        StringBuilder countQ = new StringBuilder("Select count (e.idEvent) from CulturalEvent e");
        addWhere(countQ,f);
        Query countQuery = entityManager.createQuery(countQ.toString());
        remplaceParameters(countQuery,f);
        Long countResults = (Long) countQuery.getSingleResult();
        return (int) (Math.ceil((double)countResults /(double) f.getLimitPerPage()));
    }
    private void addWhere(StringBuilder queryString,Filter f){
        queryString.append(" WHERE 1 = 1");
        if(!"".equals(f.getPostalCode()) && f.getPostalCode()!=null){
            queryString.append(" AND e.location.postalCode = :postalCode");
        }
        if (!"".equals(f.getCategory()) && f.getCategory()!=null){
            queryString.append(" AND e.category = :category");
        }
        if (!"".equals(f.getPopulation()) && f.getPopulation()!=null){
            queryString.append(" AND e.population = :population");
        }
        if (!"".equals(f.getTitle()) && f.getTitle()!=null){
            queryString.append(" AND e.title LIKE :title");
        }
        if (!"".equals(f.getStartDate()) && f.getStartDate() != null){
            queryString.append(" AND e.startDate = :startDate");
        }
    }
    private void remplaceParameters(Query query,Filter f){
        if (!"".equals(f.getCategory())&& f.getCategory() != null){
            query.setParameter("category", f.getCategory());
        }
        if (!"".equals(f.getPopulation())&& f.getPopulation() != null){
            query.setParameter("population", f.getPopulation());
        }
        if (!"".equals(f.getTitle())&& f.getTitle() != null){
            query.setParameter("title", "%"+f.getTitle()+"%");
        }
        if (!"".equals(f.getStartDate()) && f.getStartDate() != null){
            query.setParameter("startDate",f.getStartDate() );
        }
        if (!"".equals(f.getPostalCode()) && f.getPostalCode()!=null){
            query.setParameter("postalCode", f.getPostalCode());
        }
    }
    @Override
    public List<String> getListCategory () {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Query query = entityManager.createQuery("SELECT category FROM CulturalEvent group by category");
        return query.getResultList();
    }

    @Override
    public List<String> getListPopulation () {
        entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Query query = entityManager.createQuery("SELECT population FROM CulturalEvent group by population");
        return query.getResultList();
    }
}
