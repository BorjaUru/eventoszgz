package models.dao;

import beans.User;

import java.util.List;

public interface UserDao {

    List<User> checkLogin(String user, String pass) throws Exception;
}
