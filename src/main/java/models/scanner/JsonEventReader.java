package models.scanner;

import beans.CulturalEvent;
import beans.Location;
import lombok.Getter;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class JsonEventReader {
    private JsonObject jsonObject;
    private String baseUrl;

    @Getter
    private ArrayList<CulturalEvent> allEvents = new ArrayList<>();

    public JsonEventReader (String baseUrl){
        this.baseUrl = baseUrl;
    }

    /** Este código descarga los 3000 eventos!
     *  usar SOLO 1 vez para rellenar la bd */
    public void importFullJson() throws IOException{
        // ?start=3151 - ?rows=3113
        int sizeJson = getSizeJson(baseUrl);
        URL url = new URL(baseUrl+"?rows=" +  sizeJson+"&srsname=wgs84");
        saveToDatabase(url);
    }

    public void import50Json() throws IOException{
        URL url = new URL(baseUrl);
        saveToDatabase(url);
    }

    // TODO add request filtering by creationDate
    public void importLatestJson(int quantity) throws IOException{
        // import last quantity events
    }

    private void saveToDatabase(URL url) throws IOException{
        InputStream is = url.openStream();
        JsonReader rdr = Json.createReader(is);

        JsonObject completeJson = rdr.readObject();
        rdr.close();
        is.close();
        JsonArray allJson = completeJson.getJsonArray("result");
        for(int x = 0; x< allJson.size(); x++){
            System.out.println("leyendo evento: "+(x+1));
            jsonObject = allJson.getJsonObject(x);
            createEvent(new CulturalEvent());
        }
        getSomeEventInfo();
    }

    private void createEvent(CulturalEvent event){
        event.setIdEvent(jsonObject.getInt("id"));
        event.setTitle(jsonObject.getString("title"));
        try {
            event.setCategory(jsonObject.getJsonArray("category")
                    .getJsonObject(0)
                    .getString("title"));
        }catch(NullPointerException n){
            System.out.println("\tNullPointerException in category");
        }
        try {
            event.setStartDate(jsonObject.getString("startDate").split("T")[0]);
        }catch(NullPointerException n){
            System.out.println("\tNullPointerException in startDate");
        }
        try {
            event.setEndDate(jsonObject.getString("endDate").split("T")[0]);
        }
        catch(NullPointerException n){
            System.out.println("\tNullPointerException in endDate");
        }

        event.setLocation(readLocation());
        try {
            event.setPopulation(jsonObject.getJsonArray("population")
                    .getJsonObject(0)
                    .getString("title"));
        }catch(NullPointerException n){
            System.out.println("\tNullPointerException in population");
        }
        try{
            event.setImageUrl(new URL(
                    "https:".concat(jsonObject.getString("image")
                            .replaceAll("\"", ""))));
        }catch(MalformedURLException m){
            System.out.println("\tMalformedURLException in url");
        }
        catch(NullPointerException n){
            System.out.println("\tNullPointerException in url");
        }
        try{
            event.setLatCoordinate(Double.parseDouble(jsonObject.getJsonObject("geometry")
                    .getJsonArray("coordinates")
                    .getJsonNumber(0).toString()));
        }catch(NullPointerException n){
            System.out.println("\tNullPointerException in latCoordinate");}
        try{
            event.setLonCoordinate(Double.parseDouble(jsonObject.getJsonObject("geometry")
                    .getJsonArray("coordinates")
                    .getJsonNumber(1).toString()));
        }catch(NullPointerException n){
            System.out.println("\tNullPointerException in lonCoordinate ");}
        try {
            String description = jsonObject.getString("description")
                    .replaceAll("<div>","")
                    .replaceAll("</div>","")
                    .replaceAll("<strong>","")
                    .replaceAll("</strong>","")
                    .replaceAll("<br>","")
                    .replaceAll("</br>","")
                    .replaceAll("<p>","")
                    .replaceAll("</p>","")
                    .replaceAll("<a>","")
                    .replaceAll("</a>","")
                    .replaceAll("a href=","")
                    .replaceAll("<","")
                    .replaceAll(">","")
                    .replaceAll("/","")
                    .replaceAll("\\\n","");

            if (description.length() > 200) {
                event.setDescription(( description )
                        .substring(0, 199));
            } else {
                event.setDescription(description);
            }
        }catch (NullPointerException n){
            System.out.println("\tNullPointerExcetion in description");
        }

        try{
            event.setMinAge(jsonObject.getInt("minAge"));
        }catch(Exception e){
            System.out.println("/tException in minAge");;}
        try{
            event.setMaxAge(jsonObject.getInt("maxAge"));
        }catch(Exception e){
            System.out.println("\tException in maxAge");
        }
        if(event.getPopulation() == null){
            event.setPopulation("no population");
        }
        if(event.getCategory() == null){
            event.setCategory("no category");
        }
        allEvents.add(event);
    }

    private Location readLocation (){
        Location location = new Location();
        JsonObject jsonLocation = null;
        try {
            jsonLocation = jsonObject.getJsonArray("subEvent")
                    .getJsonObject(0).getJsonObject("location");
        }catch(NullPointerException n){
            System.out.println("\tNullPointerException en jsonLocation");
        }
        if (jsonLocation != null) {
            location.setIdLocation(jsonLocation.getInt("id", 0));
            location.setPlace(jsonLocation.getString("title", ""));
            String publicTransport = jsonLocation.getString("publicTransport", "");
            if(publicTransport.length()>250){
                location.setPublicTransport((publicTransport).substring(0,248));
            }else{
                location.setPublicTransport(publicTransport);
            }
            location.setAddress(jsonLocation.getString("streetAddress", ""));
            location.setPostalCode(jsonLocation.getString("postalCode", ""));
        }
        return location;
    }

    // TODO delete
    private void getSomeEventInfo (){
        System.out.println("\n\n\t\t****************************");
        System.out.println("\t\t"+allEvents.size()+" events");
        System.out.println("\t\t****************************\n");
        for(CulturalEvent e : allEvents){
            System.out.println("\ttitle: "+e.getTitle());
            System.out.println("\tcategory: "+e.getCategory());
            System.out.println("\tstartDate: "+e.getStartDate());
            System.out.println("\timageUrl: "+e.getImageUrl());
            System.out.println("\t---------------------------------------------------------------------------------");
        }
        System.out.println();
    }

    private int getSizeJson(String stringUrl) throws IOException{
        URL url = new URL(stringUrl);
        InputStream is = url.openStream();
        JsonReader rdr = Json.createReader(is);
        JsonObject completeJson = rdr.readObject();
        int result = completeJson.getInt("totalCount");
        rdr.close();
        is.close();
        return result;
    }
}
