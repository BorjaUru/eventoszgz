package models.service;

import beans.User;
import models.dao.UserDao;
import models.dao.UserDaoImpl;

import java.util.List;

public class LoginServiceImpl implements LoginService {

    UserDao userDao = new UserDaoImpl();

    @Override
    public User checkLogin(String nick, String pass) throws Exception {
        List<User> checkLogin = userDao.checkLogin(nick, pass);
        if (checkLogin.size()!=1){
            throw new Exception("this User and password are incorrect");
        }
        return checkLogin.get(0);
    }
}
