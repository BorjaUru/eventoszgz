package models.service;

import beans.User;

public interface LoginService {
    User checkLogin(String nick, String pass) throws Exception;
}
