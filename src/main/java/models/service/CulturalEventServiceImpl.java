package models.service;

import beans.CulturalEvent;
import beans.Filter;
import models.dao.Dao;
import models.dao.EventDao;
import models.dao.EventDaoImpl;

import java.text.SimpleDateFormat;
import java.util.*;

public class CulturalEventServiceImpl implements CulturalEventService {

    private Dao eventDao =new EventDaoImpl();
    private EventDao event = new EventDaoImpl();

    /** SEARCH */

    public CulturalEvent getCulturalEvent (long id) throws Exception {
        Dao dao = new EventDaoImpl();
        Optional<CulturalEvent> opt = dao.get(id);
        if(opt.isPresent()) {
            return opt.get();
        }
        return null;
    }

    public List<CulturalEvent> listCulturalEvent() throws Exception {
        Dao dao = new EventDaoImpl();
        return dao.getAll();
    }

    public List<CulturalEvent> filterCulturalEvent(Filter filter) throws Exception {
        return ((EventDaoImpl)this.eventDao).getListParam(filter);
    }

    /** INSERTS */

    public void insertCulturalEvent (CulturalEvent culturalEvent) throws Exception {
        this.eventDao.save(culturalEvent);
    }

    public void insertMultipleEvents (ArrayList<CulturalEvent> list) throws Exception {
        // this.eventDao.save(list);
        for(CulturalEvent event:list){
            this.eventDao.save(event);
        }
    }

    /** UPDATES */

    public void updateEvent (CulturalEvent event, String[] params) throws Exception {
        this.eventDao.update(event, params);
    }

    /** DELETES */

    public void deleteCulturalEvent (CulturalEvent event) throws Exception {
        this.eventDao.delete(event);
    }

    public void deleteMultipleEvents(ArrayList<CulturalEvent> deleteList) throws Exception {
        for(CulturalEvent event:deleteList){
            eventDao.delete(event);
        }
    }

    public void deleteAllEvents() throws Exception {
        // eventDao.deleteAllEvents();
    }


    /** OTHER QUERIES */
    public List<CulturalEvent> getCulturalEventTodayList() throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        String stringDate = dt.format(new Date());
        List<CulturalEvent> list = event.findCulturalEventToday(stringDate);
        if (list.size()==0){
            throw new Exception("Error: There aren't events today");
        }
        return list;
    }

    @Override
    public List<CulturalEvent> getCulturalEventForDateList(Date date) throws Exception {
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        String stringDate = dt.format(date);
        List<CulturalEvent> list = event.findCulturalEventToday(stringDate);
        if (list.size()==0){
            throw new Exception("Error: There aren't events today");
        }
        return list;
    }

    @Override
    public List<String> filterCategory () throws Exception {
        return ((EventDaoImpl)this.eventDao).getListCategory();
    }

    @Override
    public List<String> filterPopulation () throws Exception {

        return ((EventDaoImpl)this.eventDao).getListPopulation();
    }

}
