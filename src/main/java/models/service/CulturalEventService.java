package models.service;

import beans.CulturalEvent;
import beans.Filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface CulturalEventService {

    /** SEARCHS */

    CulturalEvent getCulturalEvent(long id) throws Exception ;

    List<CulturalEvent> listCulturalEvent() throws Exception;

    List<CulturalEvent> filterCulturalEvent(Filter filter) throws Exception;

    /** INSERTS */

    void insertCulturalEvent(CulturalEvent culturalEvent) throws Exception;

    void insertMultipleEvents(ArrayList<CulturalEvent> list)throws Exception;

    /** UPDATES */

    void updateEvent (CulturalEvent event, String[] params) throws Exception;

    /** DELETES */

    void deleteCulturalEvent(CulturalEvent event) throws Exception;

    void deleteMultipleEvents(ArrayList<CulturalEvent> deleteList) throws Exception;


    /** OTHER QUERIES */

    List<CulturalEvent> getCulturalEventTodayList() throws Exception;

    List<CulturalEvent> getCulturalEventForDateList(Date date) throws Exception;


    List<String> filterCategory () throws Exception;

    List<String> filterPopulation () throws Exception;

}