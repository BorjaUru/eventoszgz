import beans.CulturalEvent;
import beans.Filter;
import models.dao.*;
import models.scanner.JsonEventReader;
import models.service.CulturalEventService;
import models.service.CulturalEventServiceImpl;
import views.ListView;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Inventario {
    public static String BASE_URL_EVENTOS = "https://www.zaragoza.es/sede/servicio/cultura/evento/list.json";
    public static void main (String[] args) {
        andres();
        // borja();
    }

    /** main andres */
    private static void andres(){
        try {

            JsonEventReader je = new JsonEventReader(BASE_URL_EVENTOS);
            je.importFullJson(); //Usar importFullJson solo una vez, para rellenar la bd
            //je.import50Json();
            ArrayList<CulturalEvent> allEvents = je.getAllEvents();


             CulturalEventService culturalEventService = new CulturalEventServiceImpl();
//            List<CulturalEvent> list = culturalEventService.listCulturalEvent();
            // CulturalEvent ev = culturalEventService.getCulturalEvent(4403);
             culturalEventService.insertMultipleEvents(allEvents);
            //culturalEventService.listCulturalEvent();

            // Dao dao = new EventDaoImpl();
            //dao.delete(allEvents);
            //dao.save(allEvents.get(1));
            // dao.save(allEvents);

//            for(CulturalEvent ev : list) {
//                System.out.println(ev);
//            }


        }catch (Exception e){e.printStackTrace();}
    }

    /** main borja */
   /* private static void borja(){
        ListView a= new ListView(new Sesion(),new Filter());
        a.pack();
        a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        a.setVisible(true);
    }*/
}
