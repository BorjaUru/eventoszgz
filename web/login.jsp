<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
          crossorigin="anonymous">
    <%@include file="includeHeader.jsp"%>
    <title>Login</title>
</head>
<body class="">
<%@include file="header.jsp"%>
<div class="wrapper">
    <div class="main-panel">
        <div class="content">
            <div class="row" style="margin-top: 70px">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <form>
                                <div class="row" style="margin: auto">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-11 pr-md-1">
                                                <div class="form-group">
                                                    <label style="font-size: 1rem;">User</label>
                                                    <input id="logUser" class="form-control" placeholder="Username">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-11 pr-md-1">
                                                <div class="form-group">
                                                    <label style="font-size: 1rem;">Password:</label>
                                                    <input type="password" id="logPass" class="form-control" placeholder="Password">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="margin: auto">
                                        <div class="row">
                                            <div class="col-md-11 pr-md-1">
                                                <button id="passButton" class="btn btn-fill btn-primary">
                                                    ENTER
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-left: 25%">
                                    <p style="font-size: 10px; color:lightgrey;">Please, enter you username and pasword</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="footer.jsp"%>
    </div>
</div>
<%@include file="includeFooter.jsp"%>
</body>
<script src="./js/header.js" type="text/javascript"></script>
<script src="./js/login.js" type="text/javascript"></script>
</html>
