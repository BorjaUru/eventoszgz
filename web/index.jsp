<!DOCTYPE html>
<html lang="es">

<html>
<head>
    <title> Index </title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <!-- datepicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css" />
    <%@include file="includeHeader.jsp"%>
</head>
<body class="">
<%@include file="header.jsp"%>
<div class="wrapper">.
    <div class="sidebar">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="./index.jsp" class="simple-text logo-mini">
                    CE
                </a>
                <a href="./index.jsp" class="simple-text logo-normal">
                    CULTURAL EVENTS
                </a>
            </div>
            <ul class="nav">
                <li class="active ">
                    <a href="./index.jsp">
                        <i class="fas fa-home"></i>
                        <p>index</p>
                    </a>
                </li>
                <li>
                    <a href="./list.jsp">
                        <i class="far fa-list-alt"></i>
                        <p>List Events</p>
                    </a>
                </li>
                <li>
                    <a href="./detail.jsp">
                        <i class="fas fa-plus-square"></i>
                        <p>Create Events</p>
                    </a>
                </li>
                <li>
                    <a href="./user.jsp">
                        <i class="tim-icons icon-single-02"></i>
                        <p>User profile</p>
                    </a>
                </li>
                <li>
                    <a href="#" id="loadData">
                        <i class="fas fa-plus-square"></i>
                        <p>Create Events</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Cultural Events Today</h4>
                        </div>
                        <div id="eventsToday" class="card-body"></div>

                        <ul class="pagination" style="margin: 0px 15px 15px 15px;">
                            <li id="beginPage" class="page-item"><a class="page-link tim-icons icon-double-left" href="#" style="padding: 7px 7px 7px 7px"></a></li>
                            <li id="previusPage"><a class="page-link tim-icons icon-minimal-left" href="#" style="padding: 7px 7px 7px 7px"></a></li>
                            <li id="page1"><a id="textPage1" class="page-link" href="#" style="padding: 7px 7px 7px 7px">1</a></li>
                            <li id="page2"><a id="textPage2" class="page-link" href="#" style="padding: 7px 7px 7px 7px">2</a></li>
                            <li id="page3"><a id="textPage3" class="page-link" href="#" style="padding: 7px 7px 7px 7px">3</a></li>
                            <li id="page4"><a id="textPage4" class="page-link" href="#" style="padding: 7px 7px 7px 7px">4</a></li>
                            <li id="page5"><a id="textPage5" class="page-link" href="#" style="padding: 7px 7px 7px 7px">5</a></li>
                            <li id="nextPage"><a class="page-link tim-icons icon-minimal-right" href="#" style="padding: 7px 7px 7px 7px"></a></li>
                            <li id="endPage" class="page-item"><a class="page-link tim-icons icon-double-right" href="#" style="padding: 7px 7px 7px 7px"></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div id="datepicker"></div>
                        <div class="col-md-12">
                            <div class="card-header">
                                <h5 align="center" class="card-title">Location list events</h5>
                            </div>
                            <div style="border:1px solid #999" id="map" width="100%" height="150px"><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="footer.jsp"%>
    </div>
</div>

<div id="plantilla" style="display:none">
    <div class="alert alert-info">
        <div class="row">
            <div class="col-md-12">
                <h5 style="color:floralwhite"><b style="color:black; font-size: 14px">%title%</b></h5>
            </div>
            <div class="col-md-3">
                <img src="%img%" height="80px"/>
            </div>
            <div class="col-md-9">
                <p style="color:floralwhite; font-size: 12px">%location%, %address%</p>
                <p style="color:floralwhite; font-size: 12px">%description%</p>
            </div>
        </div>
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"></script>
<script>
    var map = L.map('map').setView([41.6517501,-0.89], 11);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery Â© ' +
            '<a href="http://cloudmade.com">CloudMade</a>',
        maxZoom: 20
    }).addTo(map);
    L.control.scale().addTo(map);
    var markersLayer = new L.LayerGroup();
</script>
<!-- script externos -->
<%@include file="includeFooter.jsp"%>
<!-- DatePicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<!-- script propios -->
<script src="./js/header.js" type="text/javascript"></script>
<script src="./js/index.js" type="text/javascript"></script>
</body>
</html>