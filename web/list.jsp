
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>
        list
    </title>
    <%@include file="includeHeader.jsp"%>
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- BOOTSTRAP-TABLE -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <!-- CSS Files -->
    <link href="./assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.14.2/bootstrap-table.css" />

</head>

<body class="">
<%@include file="header.jsp"%>
<div class="wrapper">
    <div class="sidebar">
        <div class="sidebar-wrapper">
            <div class="logo">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="tim-icons icon-zoom-split"></i>
                        </div>
                    </div>
                    <input type="text" id="title" class="form-control" placeholder="Title....">
                </div>
            </div>
            <ul class="nav">
                <li>
                    <div class="form-group ">
                        <p class="text-center">Category</p>
                        <select id="category" class="form-control">
                            <option value="-1" >Choose...</option>
                        </select>
                    </div>
                </li>
                <li>
                    <div class="form-group ">
                        <p class="text-center">Population</p>
                        <select id="population" class="form-control">
                            <option value="-1">Choose...</option>
                        </select>
                    </div>
                </li>
                <li>
                    <input id="datepicker" class="form-control"/>
                </li>
                <li>
                    <input type="text" id="cp" class="form-control" placeholder="Postal Code....">
                </li>
                <li style="text-align: center">
                    <div>
                        <button class="btn btn-default animation-on-hover " type="button" onclick="eventFilter()">Search</button>
                    </div>

                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header">
                            <h4 class="card-title"> Cultural Events</h4>
                            <div class="text-right">
                                <button type="button" rel="tooltip" class="btn btn-success btn-link btn-icon btn-sm">
                                    <i class="tim-icons icon-simple-add" onclick="ver('')"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table tablesorter text-center" id="table">
                                        <thead class="text-primary">
                                        <tr>
                                            <th data-field="title">Title</th>
                                            <th data-field="category">Category</th>
                                            <th data-field="startDate">Start Date</th>
                                            <th data-field="endDate">End Date</th>
                                            <th data-field="population">Population</th>
                                            <th data-field="location.postalCode">CP</th>
                                            <th data-formatter="verOpciones">Actions</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="text-center">
                                <button id="firstPage" type="button" rel="tooltip" class="btn btn-success btn-link btn-icon btn-sm" disabled="true">
                                    <i class="tim-icons icon-double-left" onclick="pag('-2')"></i>
                                </button>
                                <button id="previousPage" type="button" rel="tooltip" class="btn btn-success btn-link btn-icon btn-sm" disabled="true">
                                    <i class="tim-icons icon-minimal-left" onclick="pag('-1')"></i>
                                </button>
                                <button id="nextPage" type="button" rel="tooltip" class="btn btn-success btn-link btn-icon btn-sm" disabled="true">
                                    <i class="tim-icons icon-minimal-right" onclick="pag('1')"></i>
                                </button>
                                <button id="lastPage" type="button" rel="tooltip" class="btn btn-success btn-link btn-icon btn-sm" disabled="true">
                                    <i class="tim-icons icon-double-right" onclick="pag('2')"></i>
                                </button>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Events pag</label>
                                    <input type="number" id="elementXPag" class="form-control" value="6" oninput="changePag()">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <jsp:include page="footer.jsp" />
    </div>
</div>
<%@include file="includeFooter.jsp"%>
<!-- BOOTSTRAP-TABLE -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.14.2/bootstrap-table.js"></script>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="./js/header.js" type="text/javascript"></script>
<script src="js/list.js"></script>
</body>
</html>