var mode = 0;
var previous;
$(document).ready(function(){
    //Guardamos datos originales
    $.getJSON({
        url: "DetailControllerServlet",
        data: {
            todo: "search"
        },
        success: function (resJSON) {
            previous = resJSON;
            fillFields(resJSON);
            if(resJSON.mode.localeCompare("creation")===0){
                notificate('info','MODE: CREATION');
                updateButton();
            }
            else if(resJSON.mode.localeCompare("visualization")===0){
                notificate('info','MODE: VISUALIZATION');
            }else{
                notificate('info','MODE: ???????????');
            }
        },
        error: function () {
            notificate('error','Error charging page');
        }
    });
    disableFields();
    $("#btn-save").hide();
    $("#btn-cancel").hide();
});

// BUTTONS
function updateButton(){
    $("#btn-update").hide();
    $("#btn-save").show();
    $("#btn-cancel").show();
    $("#btn-back").hide();
    enableFields();
}

function saveButton() {

    $.getJSON({
        url: "DetailControllerServlet",
        data: {
            todo: "save",
            id_event: $('#id-event').val(),
            description: $('#description').val(),
            title_event: $('#title-event').val(),
            category: $('#category').val(),
            population: $('#population').val(),
            start_date: $('#start-date').val(),
            end_date: $('#end-date').val(),
            place: $('#place').val(),
            transport: $('#transport').val(),
            address: $('#address').val(),
            postal_code: $('#postal-code').val()
        },
        success: function (resJSON) {
            notificate('success','Datos guardados correctament');
            previous = resJSON;
            fillFields(resJSON);
        },
        error: function () {
            notificate('error','Error saving data');
            fillFields(previous);
        }
    });
    // fillFields(previous);
    $("#btn-update").show();
    $("#btn-save").hide();
    $("#btn-cancel").hide();
    $("#btn-back").show();
    disableFields();
}

function cancelButton() {
    $("#btn-update").show();
    $("#btn-save").hide();
    $("#btn-cancel").hide();
    $("#btn-back").show();
    fillFields(previous);
    disableFields();
}

function backButton() {
    disableFields();
    window.location.href = "../list.jsp?reset=0";
}

// MODES
function visorMode() {
    // Solo ver (usuario sin registrar)
    notificate('info','Este es el ícono de Facebook!');
    $("#btn-update").hide();
    $("#btn-save").hide();
    $("#btn-cancel").hide();
    $("#btn-back").show();
}

function creatorMode() {
    // Ver + crear (usuario registrado)
    notificate('danger','Este es el ícono de Twitter!');
    $("#btn-update").hide();
    $("#btn-save").show();
    $("#btn-cancel").show();
    $("#btn-back").show();
}

function editorMode() {
    // Ver + crear + editar + borrar (ADMIN)
    notificate('info','Este es el ícono de Google+ aunq este ya no lo usa ni Pirri!');
    $("#btn-update").show();
    $("#btn-save").show();
    $("#btn-cancel").show();
    $("#btn-back").show();
}


// readonly - disabled
function disableFields() {
    $('#id-event').attr("disabled", true);
    $('#description').attr("disabled", true);
    $('#title-event').attr("disabled", true);
    $('#category').attr("disabled", true);
    $('#population').attr("disabled", true);
    $('#start-date').attr("disabled", true);
    $('#end-date').attr("disabled", true);
    $('#place').attr("disabled", true);
    $('#transport').attr("disabled", true);
    $('#address').attr("disabled", true);
    $('#postal-code').attr("disabled", true);
}

function enableFields() {
    $('#title-event').attr("disabled", false);
    $('#description').attr("disabled", false);
    $('#category').attr("disabled", false);
    $('#population').attr("disabled", false);
    $('#start-date').attr("disabled", false);
    $('#end-date').attr("disabled", false);
    $('#place').attr("disabled", false);
    $('#transport').attr("disabled", false);
    $('#address').attr("disabled", false);
    $('#postal-code').attr("disabled", false);
}

function fillFields(origin) {
    $('#id-event').val(origin.ev.idEvent);
    $('#description').val(origin.ev.description);
    $('#title-event').val(origin.ev.title);
    $('#category').val(origin.ev.category);
    $('#population').val(origin.ev.population);
    $('#start-date').val(origin.ev.startDate);
    $('#end-date').val(origin.ev.endDate);
    $('#place').val(origin.ev.location.place);
    $('#transport').val(origin.ev.location.publicTransport);
    $('#address').val(origin.ev.location.address);
    $('#postal-code').val(origin.ev.location.postalCode);
    $('#event-img').attr("src", origin.ev.imageUrl);
}
