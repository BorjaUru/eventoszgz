$(document).ready(function(){
    var url = window.location.href;
    if (url.indexOf("index.jsp")!=-1){
        $("#headerSubtitleIndex").show();
        $("#headerSubtitleList").hide();
        $("#headerSubtitleLogin").hide();
        $("#separatorDetail").hide();
        $("#headerSubtitleDetail").hide();

    } else if (url.indexOf("list.jsp")!=-1){
        $("#headerSubtitleIndex").hide();
        $("#headerSubtitleList").show();
        $("#headerSubtitleLogin").hide();
        $("#separatorDetail").hide();
        $("#headerSubtitleDetail").hide();

    } else if (url.indexOf("login.jsp")!=-1){
        $("#headerSubtitleIndex").hide();
        $("#headerSubtitleList").hide();
        $("#headerSubtitleLogin").show();
        $("#separatorDetail").hide();
        $("#headerSubtitleDetail").hide();

    }else if (url.indexOf("detail.jsp")!=-1) {
        $("#headerSubtitleIndex").hide();
        $("#headerSubtitleList").show();
        $("#headerSubtitleLogin").hide();
        $("#separatorDetail").show();
        $("#headerSubtitleDetail").show();
        $("#headerSubtitleList").attr("href", "./list.jsp");
        $("#headerSubtitleList").attr("style","")
    } else {
        $("#headerSubtitleIndex").show();
        $("#headerSubtitleList").hide();
        $("#headerSubtitleLogin").hide();
        $("#separatorDetail").hide();
        $("#headerSubtitleDetail").hide();
    }

    $("#logoutButton").on("click", function() {
        $.getJSON({
            url: "Login/logout",
            success: function(json){
                if (json.reload != null){
                    $("#login").show();
                    $("#logout").hide();
                }
            },
            error: function (error) {
                alert("Error " + error.responseText);
            }
        });
    });

    $.getJSON({
        url: "Login/userSession",
        success: function (json) {
            if (json.userSesion.role != 0) {
                $("#login").hide();
                $("#logout").show();
            } else {
                $("#login").show();
                $("#logout").hide();
            }
        },
        error: function (error) {
            alert("Error " + error.responseText);
        }
    })
});

// 'danger', 'primary','info',
// 'success','warning','default'
function notificate(type, message) {
    $.notify(
        {
            //options
            icon: "tim-icons icon-bell-55",
            message: message
        },
        {
            type: type,
            placement: {
                from: 'top',
                align: 'center'
            }
        });
}
