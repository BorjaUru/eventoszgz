var login={
    checkUser: function (){
        $.getJSON({
            url: "Login/login",
            data: {
                user: $("#logUser").val(),
                pass: $("#logPass").val()
            },
            success: function (json) {
                if (json.error != null) {
                    alert(json.error);
                }

                if (json.redirect != null) {
                    window.location.href = "../index.jsp";
                }
            },
            error: function (error) {
                alert("Error " + error.responseText);
            }
        })
    }
};

$(document).ready(function(){
    $("#passButton").on("click", function() {
        login.checkUser();
    });
});