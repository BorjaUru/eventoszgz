var range = 5;
var totalPages;
var currentPage;
var listEvents = [];

var index={
    loadDatepicker : function(){
        $('#datepicker').datepicker({
            firstDay: 1,
            weekStart: 0,
            todayHighlight: true
        }).datepicker('update', new Date()).css("color","#e9ecef").css("font-size","10.5px")
            .on('changeDate',function(e){
                index.bringEventsForDate(e.date)
            });
    },
    bringEvents : function(){
        $.ajax({
            type: "GET",
            url: "Index/loadEventsToday",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function(json){
                listEvents=[];
                if (json.error != null){
                    alert(json.error);
                }
                if (json.eventList != null){
                    listEvents = json.eventList;
                    totalPages = Math.floor((json.eventList.length-1)/range)+1;
                    currentPage = 1;
                    index.loadEvents(listEvents);
                    index.drawPagination();
                }
            },
            error : function(error){
                alert("Error " + error.responseText);
            }
        })
    },
    bringEventsForDate : function(date){
        $.ajax({
            type: "GET",
            url: "Index/loadEventsForDate",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {"date": date},
            success: function(json){
                if (json.error != null){
                    alert(json.error);
                }
                if (json.eventList != null){
                    listEvents = json.eventList;
                    totalPages = Math.floor((json.eventList.length-1)/range)+1;
                    currentPage = 1;
                    index.loadEvents(listEvents);
                    index.drawPagination();
                }
            },
            error : function(error){
                alert("Error " + error.responseText);
            }
        })
    },
    loadEvents: function(eventList){
        var marker;
        var htmlCopy="";
        var endEventToShowInPagination;
        if (range*currentPage <= listEvents.length){
            endEventToShowInPagination = range*currentPage;
        } else {
            endEventToShowInPagination = listEvents.length;
        }
        markersLayer.clearLayers();
        for (var i=range*(currentPage-1); i<endEventToShowInPagination; i++)  {
            var imgCommon = "https://pbs.twimg.com/profile_images/993967511099068416/F3HK-M-4_400x400.jpg";
            var title = (eventList[i].title != null ? eventList[i].title: "No Tittle");
            var description = (eventList[i].description != null ? eventList[i].description : "No description");
            var place = (eventList[i].location.place != null ? eventList[i].location.place : "No Place");
            var address = (eventList[i].location.address != null ? eventList[i].location.address : "No Address");
            var imgUrl = (eventList[i].imageUrl != null ? eventList[i].imageUrl : imgCommon);
            htmlCopy=htmlCopy+index.copiar(title, description, place,address,imgUrl);
            if ((eventList[i].latCoordinate != 0) && (eventList[i].lonCoordinate != 0)) {
                marker = new L.marker([(eventList[i].lonCoordinate), (eventList[i].latCoordinate)], {draggable: false})
                    .bindPopup("<b>" +title+"</b> <a href=\"#\" onclick=\"showDetail("+eventList[i].idEvent+")\">show detail</a>");
                markersLayer.addLayer(marker);
            }
            markersLayer.addTo(map);
        }
        $("#eventsToday").html(htmlCopy);
        $(".leaflet-container").css("font-size", 8);
        $(".leaflet-control-attribution.leaflet-control").css("font-size", 8);
        $(".leaflet-control-zoom.leaflet-bar.leaflet-control").css("margin", "10px 0px 0px 5px");
        $(".leaflet-touch .leaflet-bar a").css({"width":"15px","height":"15px","line-height":"15px"});
        $(".leaflet-touch .leaflet-control-zoom-in").css("font-size",9);
        $(".leaflet-touch .leaflet-control-zoom-out").css("font-size",9);
        $(".leaflet-control-scale-line").css("font-size",9);
        $(".leaflet-container.leaflet-touch.leaflet-retina.leaflet-fade-anim.leaflet-grab.leaflet-touch-drag.leaflet-touch-zoom").css("height","300px");
    },
    copiar: function(title, description, location, address, img){
        var v=$("#plantilla").html();
        v=v.replace("%title%", title);
        v=v.replace("%description%",description);
        v=v.replace("%location%",location);
        v=v.replace("%address%",address);
        v=v.replace("%img%",img);
        return v;
    },
    drawPagination: function() {
        var startPage;
        var endPage;
        var namePageId = ["#page1", "#page2", "#page3", "#page4", "#page5"];
        var textNamePageId = ["#textPage1", "#textPage2", "#textPage3", "#textPage4", "#textPage5"];

        if (totalPages < range) {
            startPage = 1;
            endPage = totalPages;
        } else if ((currentPage < (range / 2) + 1) && (totalPages >= range)) {
            startPage = 1;
            endPage = (startPage + range) - 1;
        } else if ((currentPage >= (totalPages - (range / 2))) && (totalPages >= range)) {
            startPage = Math.floor(totalPages - range + 1);
            endPage = (startPage + range) - 1;
        } else {
            startPage = (currentPage - Math.floor(range / 2));
            endPage = (startPage + range) - 1;
        }

        var cont = 0;
        for (var i = startPage; i <= endPage; i++) {
            if (i == currentPage) {
                $(textNamePageId[cont]).html(i);
                $(namePageId[cont]).attr("class", "page-item active");
                cont = cont+1;
            } else {
                $(textNamePageId[cont]).html(i);
                $(namePageId[cont]).attr("class", "page-item");
                cont = cont+1;
            }
        }
        for (var i = cont; i < range; i++) {
            $(namePageId[i]).hide();
        }

        if (currentPage===1){
            $("#beginPage").attr("class","page-item disabled");
            $("#previusPage").attr("class","page-item disabled");
        } else {
            $("#beginPage").attr("class","page-item");
            $("#previusPage").attr("class","page-item");
        }

        if (currentPage===totalPages){
            $("#endPage").attr("class","page-item disabled");
            $("#nextPage").attr("class","page-item disabled");
        } else {
            $("#endPage").attr("class","page-item");
            $("#nextPage").attr("class","page-item");
        }
    },
    beginPage: function(){
        currentPage = 1;
        index.loadEvents(listEvents);
        index.drawPagination();
    },
    previousPage: function(){
        if (currentPage > 1){
            currentPage = currentPage-1;
            index.loadEvents(listEvents);
            index.drawPagination();
        }
    },
    nextPage: function(){
        if (currentPage<totalPages){
            currentPage = currentPage+1;
            index.loadEvents(listEvents);
            index.drawPagination();
        }
    },
    endPage: function(){
        currentPage = totalPages;
        index.loadEvents(listEvents);
        index.drawPagination();
    },
    selectPage: function(page) {
        if (page != currentPage) {
            currentPage = page;
            index.loadEvents(listEvents);
            index.drawPagination();
        }
    },
    loadData: function(){
        $.ajax({
            type: "GET",
            url: "Index/loadEventsForDate",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {"date": date},
            success: function (json) {
                alert("Message " + json.msg)
            },
            error: function (error) {
                alert("Error " + error.responseText);
            }
        })
    }
}

$(document).ready(function(){
    index.loadDatepicker();
    index.bringEvents();
    $(".datepicker-inline").css("margin", "auto");
    $("#beginPage").click(function() {
        index.beginPage();
    });
    $("#previusPage").click(function() {
        index.previousPage();
    });
    $("#nextPage").click(function() {
        index.nextPage();
    });
    $("#endPage").click(function() {
        index.endPage();
    });
    $("#page1").click(function() {
        index.selectPage($("#textPage1").html());
    });
    $("#page2").click(function() {
        index.selectPage($("#textPage2").html());
    });
    $("#page3").click(function() {
        index.selectPage($("#textPage3").html());
    });
    $("#page4").click(function() {
        index.selectPage($("#textPage4").html());
    });
    $("#page5").click(function() {
        index.selectPage($("#textPage5").html());
    });
});

function showDetail(id){
    $.getJSON({
        url: "ListControllerImpl/ver",
        data: {
            theid: id

        },
        success: function (resJSON) {

            window.location.href = "detail.jsp";
        },
        error: function () {
            alert('Error imp');
        }
    });

    $("#loadData").on("click",function(){
        index.loadData();
    });
}




