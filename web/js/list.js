var numPag;
var lastPage;
function verOpciones(value,row,index){
    var cel ="<button type=\'button\', rel=\'tooltip\' class=\'btn btn-info btn-link btn-icon btn-sm\'><i class=\'tim-icons icon-double-right\' onclick='ver("+row.idEvent+")'></i></button>";
    cel= cel + "<button type=\'button\', rel=\'tooltip\' class=\'btn btn-danger btn-link btn-icon btn-sm\'><i class=\'tim-icons icon-simple-remove\' onclick='borrar("+row.idEvent+")'></i></button>";
    return cel;
}
function ver(id){
    $.getJSON({
        url: "ListControllerImpl/ver",
        data: {
            theid: id

        },
        success: function (resJSON) {

            window.location.href = "detail.jsp";
        },
        error: function () {
            alert('Error imp');
        }
    });
}
function changePag() {
    $.getJSON({
        url:"ListControllerImpl/changePag",
        data :{
            limitPerPage: $('#elementXPag').val()
        },
        success : function (json) {
            succes(json);
        },
        error : function (error) {
            alert("Error"+error.responseText);
        }
    });
}
function succes(json) {
    if(json.error!= null){
        alert("Error");
    }
    if(json.dataListaCulturalEvent!= null){
        $("#table").bootstrapTable("destroy");
        $("#table").bootstrapTable({
            data:json.dataListaCulturalEvent
        });
    }
    var f = json.filter;
    if(f.category!==""){
        $('#category').val(f.category);
    }
    if(f.population!==""){
        $('#population').val(f.population);
    }
    $('#cp').val(f.postalCode);
    $('#datepicker').val(f.startDate);
    $('#title').val(f.title);
    updatePag(json);
}
function borrar(id){
    $.getJSON({
        url:"ListControllerImpl/borrar",
        data :{
            theid: id
        },
        success : function (json) {
           succes(json);
        },
        error : function (error) {
            alert("Error"+error.responseText);
        }
    });

}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
function pag(opcion) {
    if(opcion==="-2"){
        numPag=1;
    }else if(opcion==="-1"){
        numPag=numPag-1;
    }else if(opcion==="1"){
        numPag=numPag+1;
    }else if(opcion==="2"){
        numPag=-1;
    }
    $.getJSON({
        url:"ListControllerImpl/pag",
        data :{
            numPag: numPag
        },
        success : function (json) {
           succes(json);
        },
        error : function (error) {
            alert("Error"+error.responseText);
        }
    });
}
function updatePag(res) {
    var filtro = res.filter;
    numPag=filtro.numPage;
    lastPage=filtro.lastPage;
    if(numPag!==1){
        $("#firstPage").attr("disabled",false);
        $("#previousPage").attr("disabled",false);
    }else{
        $("#firstPage").attr("disabled",true);
        $("#previousPage").attr("disabled",true);
    }
    if(numPag!==lastPage){
        $("#lastPage").attr("disabled",false);
        $("#nextPage").attr("disabled",false);
    }else{
        $("#lastPage").attr("disabled",true);
        $("#nextPage").attr("disabled",true);
    }
}
function eventFilter(){
    var category =  $('#category').val();
    var population =  $('#population').val();
    if(population==='-1'){
        population = "";
    }else{
        population = $('#population option:selected').text();
    }
    if(category==='-1'){
        category = "";
    }else{
        category = $('#category option:selected').text();
    }

    $.getJSON({
        url:"ListControllerImpl/filtrar",
        data :{
            cp: $('#cp').val(),
            fecha: $('#datepicker').val(),
            population: population,
            category: category,
            title: $('#title').val()
        },
        success : function (json) {
            succes(json);
        },
        error : function (error) {
            alert("Error"+error.responseText);
        }
    });
}
$(document).ready(function() {
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy/mm/dd'
    });
    $().ready(function() {
        var reset=1;
        if(getUrlParameter('reset')==='0' ){
            reset =getUrlParameter('reset');
        }
        history.pushState(null, "", "list.jsp");
        $.getJSON({
            url: "ListControllerImpl/cargar",
            data :{
                reset: reset
            },
            contentType: 'application/json; charset=UTF-8',
            success: function(res){
                $.each(res.dataCategory, function (keys,item) {
                    $('<option>').val(item).text(item).appendTo($('#category'));
                });
                $.each(res.dataPopulation, function (keys,item) {
                    $('<option>').val(item).text(item).appendTo($('#population'));
                });
                succes(res);
            },
            error: function(){
                alert("Error imprevisto");
            }
        });


    });
    $(".gj-datepicker button").attr("class", "bg-dark border-0");

});

