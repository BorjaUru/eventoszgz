<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Detail</title>
    <%@include file="includeHeader.jsp"%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css" />
</head>
<body>
<%@include file="header.jsp"%>
<div class="wrapper">
    <div class="main-panel">

        <div class="content">
            <div class="row">
                <!-- MAIN PANEL -->
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title"> DETAIL</h5>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-3 pr-md-10">
                                        <div class="form-group">
                                            <label>ID</label>
                                            <input id="id-event" type="text" class="form-control" disabled="" placeholder="ID">
                                        </div>
                                    </div>
                                    <div class="col-md-9 px-md-1">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input id="title-event" type="text" class="form-control" placeholder="Title">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 pr-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea rows=2 id="description" class="form-control" placeholder="Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-md-1">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <input id="category" type="text" class="form-control" placeholder="Category">
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-md-1">
                                        <div class="form-group">
                                            <label>Population</label>
                                            <input id="population" type="text" class="form-control" placeholder="Population">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-md-1">
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <input id="start-date" type="text" class="form-control" placeholder="Start Date">
                                        </div>
                                    </div>
                                    <div class="col-md-6 px-md-1">
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <input id="end-date" type="text" class="form-control" placeholder="End Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-md-1">
                                        <div class="form-group">
                                            <label>Place</label>
                                            <input id="place" type="text" class="form-control" placeholder="Place">
                                        </div>
                                    </div>
                                    <div class="col-md-6 px-md-1">
                                        <div class="form-group">
                                            <label>Transport</label>
                                            <input id="transport" type="text" class="form-control" placeholder="Transport">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input id="address" type="text" class="form-control" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-md-1">
                                        <div class="form-group">
                                            <label>Postal Code</label>
                                            <input id="postal-code" type="text" class="form-control" placeholder="Postal Code">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-3">
                                    <button onclick="updateButton()" id="btn-update"
                                            class="btn btn-fill btn-primary btn-info" value="Edit">
                                        <i class="tim-icons icon-pencil"></i>
                                    </button>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-2">
                                    <button onclick="saveButton()" id="btn-save"
                                            class="btn btn-fill btn-primary btn-info">
                                        <i class="far fa-save"></i>
                                    </button>
                                </div>
                                <div class="col-md-2">
                                    <button onclick="cancelButton()" id="btn-cancel"
                                            class="btn btn-fill btn-primary btn-danger">
                                        <i class="fas fa-reply"></i>
                                    </button>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <button onclick="backButton()" id="btn-back"
                                            class="btn btn-fill btn-primary btn-danger">
                                        <%--<i class="tim-icons icon-double-left"></i> Back--%>
                                        <i class="fas fa-reply-all"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- INFO PANEL -->
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="card-body">
                            <div class="card-description">
                                <img id="event-img" src="https://pbs.twimg.com/profile_images/993967511099068416/F3HK-M-4_400x400.jpg" />                         </div>
                        </div>
                        <div class="card-footer">
                            <div class="button-container">
                                <button onclick="visorMode()" href="javascript:void(0)" class="btn btn-icon btn-round btn-facebook">
                                    <i class="fab fa-facebook"></i>
                                </button>
                                <button onclick="creatorMode()" href="javascript:void(0)" class="btn btn-icon btn-round btn-twitter">
                                    <i class="fab fa-twitter"></i>
                                </button>
                                <button onclick="editorMode()" href="javascript:void(0)" class="btn btn-icon btn-round btn-google">
                                    <i class="fab fa-google-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp" />
    </div>
    <div id="rrr"></div>
</div>
<!-- BOOTSTRAP-NOTIFY LOCAL -->
<%@include file="includeFooter.jsp"%>
<script src="./js/header.js" type="text/javascript"></script>
<script src="js/detail.js" ></script>
</body>
</html>


